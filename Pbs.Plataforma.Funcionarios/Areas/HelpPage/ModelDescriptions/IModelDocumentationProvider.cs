using System;
using System.Reflection;

namespace Pbs.Plataforma.Funcionarios.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}