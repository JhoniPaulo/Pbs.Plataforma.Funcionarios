﻿using Pbs.Plataforma.Funcionarios.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using System.Web.Mvc;


namespace Pbs.Plataforma.Funcionarios.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FuncionariosController : ApiController
    {
        private Entities db = new Entities();

        private List<FuncionariosViewModel> ConvertViewModel() {
            List<FuncionariosViewModel> funcionarios = (from f in db.Funcionarios select new FuncionariosViewModel() {
                Email = f.Email,
                Especialidade = f.Especialidade,
                idEndereco = f.idEndereco,
                Nome = f.Nome,
                Rg = (int)f.Rg,
                Telefone = (int)f.Telefone
            }).ToList();
            return funcionarios;
        }

        private List<FuncionariosViewModel> PopulaViewModel(List<Models.Funcionarios> model)
        {
            List<FuncionariosViewModel> funcionarios = new List<FuncionariosViewModel>();
            foreach (var f in model)
            {
               funcionarios.Add(new FuncionariosViewModel()
               {
                    Email = f.Email,
                    Especialidade = f.Especialidade,
                    idEndereco = f.idEndereco,
                    Nome = f.Nome,
                    Rg = (int)f.Rg,
                    Telefone = (int)f.Telefone
               });
            }
            return funcionarios;
        }

        private FuncionariosViewModel PopulaViewModel(Models.Funcionarios f)
        {
            return new FuncionariosViewModel()
            {
                    Email = f.Email,
                    Especialidade = f.Especialidade,
                    idEndereco = f.idEndereco,
                    Nome = f.Nome,
                    Rg = (int)f.Rg,
                    Telefone = (int)f.Telefone
            };
        }

        // GET api/funcionarios
        [ResponseType(typeof(FuncionariosViewModel))]
        public IHttpActionResult Get(string AuthTC) {

            if (AutenticationController.CompararMd5(AuthTC))
                return Ok(ConvertViewModel());

            return Unauthorized();
        }


        // GET api/funcionarios/5
        [ResponseType(typeof(FuncionariosViewModel))]
        public IHttpActionResult Get(int id, string AuthTC)
        {
            if (!AutenticationController.CompararMd5(AuthTC))
                return Unauthorized();

            int result = db.Funcionarios.Where(x => x.idFuncionario == id).Count();
            if (result == 0)
                return BadRequest("Funcionário não Cadastrado!");

            Models.Funcionarios funcionarios = db.Funcionarios.Where(x => x.idFuncionario == id).SingleOrDefault();
            return Ok(PopulaViewModel(funcionarios));
        }

        // GET api/funcionarios/nome=nome
        [ResponseType(typeof(FuncionariosViewModel))]
        public IHttpActionResult Get(string AuthTC, string nomeUsuario, [Bind(Include = "Nome,Token,Usuario")]AutenticationViewModel model)
        {
            if (!AutenticationController.CompararMd5(AuthTC))
                return Unauthorized();

            int result = db.Funcionarios.Where(x => x.Nome == nomeUsuario).Count();
            if (result == 0)
                return BadRequest("Funcionário não Cadastrado!");

            Models.Funcionarios funcionarios = db.Funcionarios.Where(x => x.Nome == nomeUsuario).SingleOrDefault();
            return Ok(PopulaViewModel(funcionarios));
        }

        // POST api/funcionarios
        public IHttpActionResult Post(FuncionariosViewModel model, string AuthTC)
        {
            if (!AutenticationController.CompararMd5(AuthTC))
                return Unauthorized();

            if (!ModelState.IsValid)
            {
                return BadRequest("Erro! Dados inválidos ou faltantes!");
            }

            List<string> retorno = new List<string>();
            try
            {
                retorno = DAL.ApiFuncionariosDAO.RegistraFuncionario(model);
            }
            catch (DbUpdateException e)
            {
                return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest();
            }

            if (retorno.Count > 1)
            {
                foreach (var item in retorno)
                {
                    if (item == "Funcionário já Registrado.")
                        return BadRequest("Funcionário já Registrado.");

                }
                return BadRequest( "Erro! Nao cadastrado!");
            }
            return Ok("Cadastrado com sucesso!");
        }

        // PUT api/funcionarios/5
        public IHttpActionResult Put(string Nome, string NomeUpdate, string AuthTC)
        {
            if (!AutenticationController.CompararMd5(AuthTC))
                return Unauthorized();

            if (!ModelState.IsValid) return BadRequest("Erro! Dados inválidos ou faltantes!");

            Models.Funcionarios funcionario = db.Funcionarios.Where(x => x.Nome == Nome).SingleOrDefault();

            if (funcionario == null)
                return BadRequest("Funcionário não Cadastrado!");
            else
                funcionario.Nome = NomeUpdate;

            List<string> retorno = new List<string>();
            try
            {
                retorno = DAL.ApiFuncionariosDAO.AtualizaFuncionario(funcionario);
            }
            catch (Exception)
            {
                return BadRequest();
            }

            if (retorno.Count > 1)
                return BadRequest("Erro! Nao atualizado!");

            return Ok("Atualizado com sucesso!");
        }

        // DELETE api/funcionarios/?Nome=nome
        public IHttpActionResult Delete(string Nome)
        {
            if (!AutenticationController.CompararMd5(AuthTC))
                return Unauthorized();

            if (!ModelState.IsValid) return BadRequest("Erro! Dados inválidos ou faltantes!");
            Models.Funcionarios funcionario = db.Funcionarios.Where(x => x.Nome == Nome).SingleOrDefault();

            if (funcionario == null) return BadRequest("Funcionário não Cadastrado!");

            try
            {
                DAL.ApiFuncionariosDAO.DeletaFuncionario(funcionario);
                return Ok("Funcionário Deletado!");
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
