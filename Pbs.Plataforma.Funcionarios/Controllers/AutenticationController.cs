﻿using Pbs.Plataforma.Funcionarios.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;

namespace Pbs.Plataforma.Funcionarios.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AutenticationController : Controller
    {
        private static Entities db = new Entities();

        public static string SerializeToken(string token)
        {
            MD5 md5 = MD5.Create();
            
            byte[] hash = md5.ComputeHash(Encoding.ASCII.GetBytes(token));
            string posHash = Encoding.UTF8.GetString(hash);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

    public static bool CompararMd5(string Token)
    {
            string[] separa = Token.Split('/');

            AutenticationViewModel model = new AutenticationViewModel() {
                Nome = separa[1],
                Token = separa[2],
                Usuario = separa[3],
                Data = DateTime.Now
            };

            var auth = db.PlataformasAcesso.Where(x => x.Nome == model.Nome).SingleOrDefault();

            if (auth.Nome == model.Nome && auth.Token == model.Token)
                return true;
            else
                return false;

    }
}
}