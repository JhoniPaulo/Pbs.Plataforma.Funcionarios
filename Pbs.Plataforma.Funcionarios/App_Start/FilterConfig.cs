﻿using System.Web;
using System.Web.Mvc;

namespace Pbs.Plataforma.Funcionarios
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
