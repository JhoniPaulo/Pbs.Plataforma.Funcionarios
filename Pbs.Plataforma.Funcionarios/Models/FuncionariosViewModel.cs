﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Pbs.Plataforma.Funcionarios.Models
{
    public class FuncionariosViewModel
    {
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Especialidade { get; set; }
        [Required]
        public int Rg { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public int Telefone { get; set; }
        [Required]
        public int idEndereco { get; set; }
    }
}