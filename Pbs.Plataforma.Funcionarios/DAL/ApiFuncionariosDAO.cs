﻿using Pbs.Plataforma.Funcionarios.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Pbs.Plataforma.Funcionarios.DAL
{
    public class ApiFuncionariosDAO
    {
        private static Entities db = new Entities();
        public static List<string> RegistraFuncionario(FuncionariosViewModel model)
        {
            List<string> msg = new List<string>();

            try
            {
                if (model == null)
                    throw new Exception();

                if (db.Funcionarios.Where(x => x.Nome == model.Nome).Count() > 0)
                {
                    msg.Add("Funcionário já Registrado.");
                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                throw;
            }

                Models.Funcionarios funcionarios = new Models.Funcionarios(){
                Nome = model.Nome,
                Especialidade = model.Especialidade,
                Email = model.Email,
                Rg = model.Rg,
                Telefone = model.Telefone,
                idEndereco = model.idEndereco
            };
            db.Funcionarios.Add(funcionarios);
            try
            {
                db.SaveChanges();
                msg.Add("registrado");
                return msg;
            }
            catch (DbUpdateException ex)
            {
                msg.Add(ex.ToString());
                msg.Add("Não cadastrado");
                return msg;
            }
        }

        public static List<string> AtualizaFuncionario(Models.Funcionarios funcionarios)
        {
            List<string> msg = new List<string>();
            try
            {
                db.Funcionarios.Attach(funcionarios);
                db.Entry(funcionarios).State = EntityState.Modified;
                db.SaveChanges();
                msg.Add("Atualizado");
                return msg;
            }
            catch (DbUpdateException ex)
            {
                msg.Add(ex.ToString());
                msg.Add("Não cadastrado");
                return msg;
            }
        }

        public static bool DeletaFuncionario(Models.Funcionarios funcionarios) {
            try
            {
                db.Funcionarios.Attach(funcionarios);
                db.Entry(funcionarios).State = EntityState.Deleted;
                db.SaveChanges();
                return true;
            }
            catch (DbUpdateException)
            {
                return false;
            }
        }
    }
}